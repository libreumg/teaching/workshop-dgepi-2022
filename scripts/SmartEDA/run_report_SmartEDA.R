# SmartEDA SHIP report
# https://github.com/daya6489/SmartEDA
# https://CRAN.R-project.org/package=SmartEDA

if (!require(SmartEDA)) install.packages("SmartEDA")

# Load data ----

# Study of Health in Pomerania
sd1 <- readRDS(system.file("extdata", "ship.RDS", package = "dataquieR"))

# Run dynamic web report ----
ExpReport(sd1, op_file = "SmartEDA_ship_report.html")
