# Workshop: R packages to assess data quality  (DGEpi 2022)

Stephan Struckmann, Elisa Kasbohm, Joany Marino, Carsten Oliver Schmidt  

Greifswald, 26.09.2022  

## Contents

1. Introductory overview 
2. Part 1: Scope and overlap between R packages and a formal framework
3. Part 2: Assessing data quality in R using exploratory and rule-focused packages
4. Part 3: R package dataquieR – data quality assessments in health research made easy

## General overview  

### Introduction
ISO 8000 defines data quality as the “degree to which a set of inherent characteristics of data fulfils requirements” (ISO 8000). Despite its important in any research data collection, many users are uncertain as to which tools are most suitable to assess data quality. Already in the programming language R, hundreds of packages of potential interest are available. This workshop provides an applied overview on R packages of relevance, covering the following dimensions of data quality: integrity (The degree to which the data conforms to structural and technical requirements.), completeness (The degree to which expected data values are present.), consistency (The degree to which data values are free of breaks in conventions or contradictions.), and accuracy (The degree of agreement between observed and expected distributions and associations.).

### Outline
We provide an overview on three different approaches to target data quality with R. First, packages focusing on exploratory data analysis to get a fast overview on data properties while making little use of additional metadata (e.g. SmartEDA). Second, packages to conduct highly targeted rule-based checks on distinct data properties (e.g. validate), foremost within the consistency dimension. Third, packages that produce extensive data quality reports driven by metadata (e.g. dataquieR).
We illustrate all approaches based on a publicly available example data set. The exemplary data quality analysis starts without metadata, using packages of the first type, and we will revise the scope and limitations of the output. Subsequently, we will show how additional checks to detect discrepancies between observed and expected data properties can be performed using functionalities of the second package type. Finally, we will illustrate how to improve the scope and efficiency of data quality assessments with packages of the third type by setting up a metadata file, which contains information on expected data properties in a spreadsheet format.

All approaches will be illustrated based on a publicly available example data set. This data is taken from the [Study of Health in Pomerania](http://www2.medizin.uni-greifswald.de/cm/fv/ship.html) (SHIP-0, examinations from 1997 to 2001). To secure anonymity and for illustrative purposes, some noise has been introduced to the data and a 50% sample has been taken. More information is available [here](https://dfg-qa.ship-med.uni-greifswald.de/ExampleDataDescription.html). 

### Outcome
Depending on the data quality aspects of interest and on the availability of metadata, different approaches may be suitable to conduct data quality assessments. Importantly, the maturity level of the metadata determines the scope and ease of possible data quality reporting. Despite the availability of many R packages, they cannot target all potential data quality aspects of relevance.

### Prerequisites
Participants should have a beginner’s level of R and come with an up-to-date version of R and R-Studio installed on a Laptop to fully participate in exercises.
